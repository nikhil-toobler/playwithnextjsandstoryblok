import React from 'react'
import SbEditable from 'storyblok-react'

const Teaser = ({blok}) => {
  return (
    <SbEditable content={blok}>
      <div className="py-10">
        <h2 className="font-serif text-5xl text-center">{blok.headline}</h2>
        <h5 className="font-serif text-xl text-center">{blok.subtitle}</h5>
      </div>
    </SbEditable>
  )
}

export default Teaser