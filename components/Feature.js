import SbEditable from 'storyblok-react'

const Feature = ({blok}) => {
  return (
    <SbEditable content={blok}> 
      <dt class="text-lg leading-6 font-medium text-gray-900">
        {blok.name}
      </dt>
      <dd class="mt-2 text-base text-gray-500">
        {blok.para}
      </dd> 
    </SbEditable>
  )
}

export default Feature